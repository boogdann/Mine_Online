Blocks.DestoyIndex      db      -1, -1, 3, 3, 0, 3, 3, 1, 1, 0, 1, 1, 4, 1, 2, 2, 2, 4, 4, -1, 2, 2, 2, 2           
;                                0   1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18  19 20 21 22 23
; PATH - Assets/TexturesPack/

Blocks.TexturesNames:
    db  "Assets/TexturesPack/Water.png", 0
    db  "Assets/TexturesPack/Air.png", 0
    db  "Assets/TexturesPack/Log.png", 0
    db  "Assets/TexturesPack/Planks.png", 0
    db  "Assets/TexturesPack/Leaves.png", 0 
    db  "Assets/TexturesPack/Chest.png", 0 
    db  "Assets/TexturesPack/CraftingTable.png", 0
    db  "Assets/TexturesPack/Dirt.png", 0
    db  "Assets/TexturesPack/Turf.png", 0
    db  "Assets/TexturesPack/Tallgrass.png", 0
    db  "Assets/TexturesPack/Sand.png", 0
    db  "Assets/TexturesPack/Sandstone.png", 0
    db  "Assets/TexturesPack/Glass.png", 0
    db  "Assets/TexturesPack/Gravel.png", 0
    db  "Assets/TexturesPack/Cobblestone.png", 0
    db  "Assets/TexturesPack/Stone.png", 0
    db  "Assets/TexturesPack/Furnace.png", 0
    db  "Assets/TexturesPack/Wool.png", 0
    db  "Assets/TexturesPack/Bed.png", 0
    db  "Assets/TexturesPack/Planks.Bedrock", 0
    db  "Assets/TexturesPack/IronOre.png", 0
    db  "Assets/TexturesPack/GoldOre.png", 0
    db  "Assets/TexturesPack/CoalOre.png", 0
    db  "Assets/TexturesPack/DiamondOre.png", 0
    db  0